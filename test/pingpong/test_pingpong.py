#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestPingPong(FelixTestCase):

    def setUp(self):
        self.start('pong')

    def tearDown(self):
        self.stop('pong')

    def test_pingpong(self):
        try:
            timeout = 30
            bus_dir = FelixTestCase.tmp_prefix + '-bus'
            group_name = "FELIX_BUS_" + FelixTestCase.uuid
            output = subprocess.check_output(' '.join(("./felix-bus-ping", "--verbose", "--bus-dir", bus_dir, "--bus-groupname", group_name)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)
        except subprocess.TimeoutExpired as e:
            print("Timeout !")
            print(e.cmd)
            print(e.output.decode())
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
