#include <catch2/catch_test_macros.hpp>

#include "felixbus/felixbus.hpp"

using namespace felixbus;

TEST_CASE( "no fid lookup", "[error][client]" ) {

  std::filesystem::path bus_path = "data";

  FelixBus bus;
  bus.set_path(bus_path);

  std::error_code ec;

  bus.get_info(0, ec);
  REQUIRE(ec == std::errc::no_such_file_or_directory);
}
