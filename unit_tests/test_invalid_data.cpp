#include <catch2/catch_test_macros.hpp>

#include "felixbus/felixbus.hpp"

using namespace felixbus;

TEST_CASE( "invalid json data", "[invalid][client]" ) {

  std::filesystem::path bus_path = "unit_tests";
  bus_path /= "data";
  bus_path /= "invalid_data";

  if (!std::filesystem::exists(bus_path)) {
    bus_path = "felix-bus-fs" / bus_path;
    REQUIRE(std::filesystem::exists(bus_path));
  }

  FelixBus bus(bus_path);
  // bus.set_verbose(true);
  bus.set_cleanup(false);

  std::error_code ec;

  bus.get_info(0x1e0f346a00000900, ec);
  REQUIRE(ec == std::errc::no_such_file_or_directory);
}
