#include <unistd.h>

#include <catch2/catch_test_macros.hpp>

#include "felixbus/felixbus.hpp"

using namespace felixbus;

TEST_CASE( "no_raw_tcp json data", "[error][client]" ) {

  std::filesystem::path bus_path = "unit_tests";
  bus_path /= "data";
  bus_path /= "no_raw_tcp_data";

  if (!std::filesystem::exists(bus_path)) {
    bus_path = "felix-bus-fs" / bus_path;
    REQUIRE(std::filesystem::exists(bus_path));
  }

  FelixBus bus(bus_path);
  bus.set_verbose(true);
  // bus.set_cleanup(false);
  bus.set_ignore_stale(false);

  std::error_code ec;

  FelixBusInfo check_info = bus.get_info(0x1e0f346002960000, ec);
  REQUIRE(!ec);

  REQUIRE(check_info.ip == "127.0.0.1");
  REQUIRE(check_info.port == 12345);
  REQUIRE(check_info.unbuffered == false);
  REQUIRE(check_info.raw_tcp == false);
}
