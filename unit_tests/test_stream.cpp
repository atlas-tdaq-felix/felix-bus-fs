#include <sys/types.h>
#include <unistd.h>

#include <catch2/catch_test_macros.hpp>

#include "felixbus/felixbus.hpp"

#include "felixbus/felixbus.h"

using namespace felixbus;

TEST_CASE( "stream data", "[error][server][stream]" ) {

  char host[256];
  int rc = gethostname(host, 256);
  REQUIRE(rc == 0);
  std::string hostname(host);

  pid_t pid = getpid();
  REQUIRE(pid > 0);

  struct FelixBusInfo info_plain;
  info_plain.ip = "192.168.0.1";
  info_plain.port = 12345;
  info_plain.unbuffered = false;
  info_plain.pubsub = true;
  info_plain.raw_tcp = false;
  info_plain.stream = false;
  info_plain.netio_pages = 16;
  info_plain.netio_pagesize = 256;

  struct FelixBusInfo info_stream;
  info_stream.ip = "192.168.0.1";
  info_stream.port = 12345;
  info_stream.unbuffered = false;
  info_stream.pubsub = true;
  info_stream.raw_tcp = false;
  info_stream.stream = true;
  info_stream.netio_pages = 16;
  info_stream.netio_pagesize = 256;

  uint64_t plain_fid = 0x1f9f346000090000;
  uint64_t non_existing_stream_on_plain_fid = 0x1f9f346000090016;
  uint64_t stream_fid_00 = 0x1f9f346000090100;
  uint64_t stream_fid_14 = 0x1f9f346000090114;
  uint64_t implicit_stream_fid = 0x1f9f346000090128;
  uint64_t stream_fid_01 = 0x1f9f346000090101;
  uint64_t stream_fid_FF = 0x1f9f3460000901ff;
  uint64_t non_existing_fid = 0x1f9f346000090200;
  uint64_t non_existing_stream_fid = 0x1f9f346000090200;
  std::string filename = "test";

  std::filesystem::path bus_path_prefix = "unit_tests";
  bus_path_prefix /= "data";
  bus_path_prefix /= "stream";

  FelixBus bus;
  bus.set_cleanup(false);
  bus.set_verbose(false);
  bus.set_path(bus_path_prefix);

  bus.publish(plain_fid, filename, info_plain);
  bus.publish(stream_fid_00, filename, info_stream);
  bus.publish(stream_fid_14, filename, info_stream);
  bus.publish(stream_fid_01, filename, info_stream);
  bus.publish(stream_fid_FF, filename, info_stream);

  std::filesystem::path bus_path = bus_path_prefix;
  bus_path /= "FELIX";
  bus_path /= "f9";
  bus_path /= "f346";
  bus_path /= filename + ".ndjson";

  if (!std::filesystem::exists(bus_path)) {
    bus_path = "felix-bus-fs" / bus_path;
    REQUIRE(std::filesystem::exists(bus_path));
  }

  REQUIRE(felix_bus_locked(bus_path.c_str()));

  std::error_code ec;

  SECTION("Lookup of a plain fid") {
    FelixBusInfo check_info = bus.get_info(plain_fid, ec);
    REQUIRE(!ec);
    REQUIRE(check_info.ip == info_plain.ip);
    REQUIRE(check_info.port == info_plain.port);
    REQUIRE(check_info.unbuffered == info_plain.unbuffered);
    REQUIRE(check_info.raw_tcp == info_plain.raw_tcp);
    REQUIRE(check_info.stream == false);

    REQUIRE(check_info.host == hostname);
    REQUIRE(check_info.pid == pid);
  }

  SECTION("Lookup of a non-existing stream on plain fid") {
    FelixBusInfo check_info = bus.get_info(non_existing_stream_on_plain_fid, ec);
    REQUIRE(ec);
    REQUIRE(ec == std::errc::no_such_file_or_directory);
  }

  SECTION("Lookup of a stream fid 0x14") {
    FelixBusInfo check_info = bus.get_info(stream_fid_14, ec);
    REQUIRE(!ec);
    REQUIRE(check_info.ip == info_stream.ip);
    REQUIRE(check_info.port == info_stream.port);
    REQUIRE(check_info.unbuffered == info_stream.unbuffered);
    REQUIRE(check_info.raw_tcp == info_stream.raw_tcp);
    REQUIRE(check_info.stream == true);

    REQUIRE(check_info.host == hostname);
    REQUIRE(check_info.pid == pid);
  }

  SECTION("Lookup of an implicitly published stream fid 0x28") {
    FelixBusInfo check_info = bus.get_info(implicit_stream_fid, ec);
    REQUIRE(!ec);
    REQUIRE(check_info.ip == info_stream.ip);
    REQUIRE(check_info.port == info_stream.port);
    REQUIRE(check_info.unbuffered == info_stream.unbuffered);
    REQUIRE(check_info.raw_tcp == info_stream.raw_tcp);
    REQUIRE(check_info.stream == true);

    REQUIRE(check_info.host == hostname);
    REQUIRE(check_info.pid == pid);
  }

  SECTION("Lookup of stream fid 0x00") {
    FelixBusInfo check_info = bus.get_info(stream_fid_00, ec);
    REQUIRE(!ec);
    REQUIRE(check_info.ip == info_stream.ip);
    REQUIRE(check_info.port == info_stream.port);
    REQUIRE(check_info.unbuffered == info_stream.unbuffered);
    REQUIRE(check_info.raw_tcp == info_stream.raw_tcp);
    REQUIRE(check_info.stream == true);

    REQUIRE(check_info.host == hostname);
    REQUIRE(check_info.pid == pid);
  }

  SECTION("Lookup of stream fid 0x01") {
    FelixBusInfo check_info = bus.get_info(stream_fid_01, ec);
    REQUIRE(!ec);
    REQUIRE(check_info.ip == info_stream.ip);
    REQUIRE(check_info.port == info_stream.port);
    REQUIRE(check_info.unbuffered == info_stream.unbuffered);
    REQUIRE(check_info.raw_tcp == info_stream.raw_tcp);
    REQUIRE(check_info.stream == true);

    REQUIRE(check_info.host == hostname);
    REQUIRE(check_info.pid == pid);
  }

  SECTION("Lookup of stream fid 0xff") {
    FelixBusInfo check_info = bus.get_info(stream_fid_FF, ec);
    REQUIRE(!ec);
    REQUIRE(check_info.ip == info_stream.ip);
    REQUIRE(check_info.port == info_stream.port);
    REQUIRE(check_info.unbuffered == info_stream.unbuffered);
    REQUIRE(check_info.raw_tcp == info_stream.raw_tcp);
    REQUIRE(check_info.stream == true);

    REQUIRE(check_info.host == hostname);
    REQUIRE(check_info.pid == pid);
  }

  SECTION("Lookup of a non existing fid") {
    FelixBusInfo check_info = bus.get_info(non_existing_fid, ec);
    REQUIRE(ec);
    REQUIRE(ec == std::errc::no_such_file_or_directory);
  }

  SECTION("Lookup of a non existing stream fid") {
    FelixBusInfo check_info = bus.get_info(non_existing_stream_fid, ec);
    REQUIRE(ec);
    REQUIRE(ec == std::errc::no_such_file_or_directory);
  }
}
