#include <chrono>
#include <filesystem>
#include <iostream>
#include <limits.h>
#include <unistd.h>
#include <string.h>

#include <catch2/catch_test_macros.hpp>

#include "felixbus/felixbus.hpp"

#include "felix/felix_valgrind.h"

using namespace felixbus;

TEST_CASE( "performance lookup", "[performance]" ) {

  std::cout << "Generating data..." << std::endl;
  std::filesystem::path bus_path = "felix-bus-data";

  // get script directory
  char executable[PATH_MAX] = "";
  ssize_t count = readlink("/proc/self/exe", executable, PATH_MAX);
  REQUIRE(count > 0);
  REQUIRE(count < PATH_MAX);

  std::filesystem::path script = executable;
  script = script.parent_path();

  // run data script
  script = script / (is_running_in_valgrind() ? "felix-bus-generate --elink 75" : "felix-bus-generate");
  system(script.c_str());

  std::cout << "Looking up fids..." << std::endl;

  FelixBus bus(bus_path);
  bus.set_cleanup(false);
  bus.set_verbose(false);

  simdjson::dom::parser fid_parser;

  auto t_start = std::chrono::high_resolution_clock::now();
  int n = 0;
  for (int64_t fid : fid_parser.load(bus_path / "fids.json")) {
    // std::cout << fid << " " << FelixBus::int_to_hex(fid, 1) << std::endl;
    std::error_code ec;
    // FelixBusInfo info =
    bus.get_info(fid, ec);
    // std::cout << info << std::endl;
    REQUIRE(!ec);
    // REQUIRE(info.port == 12345);
    // REQUIRE(info.ip == "127.0.0.1");
    // REQUIRE(info.netio_pages == 256);
    n++;
  }
  auto t_end = std::chrono::high_resolution_clock::now();
  double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end-t_start).count();
  double time_per_lookup_ms = elapsed_time_ms / n;
  double max_time = is_running_in_valgrind() ? 30.0 : 1.0;
  std::cout << "time_per_lookup(" << n << ", max: " << max_time << " ms): " << time_per_lookup_ms << " ms." << std::endl;
  REQUIRE(time_per_lookup_ms < max_time);

}
