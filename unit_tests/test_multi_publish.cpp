#include <sys/types.h>
#include <unistd.h>

#include <catch2/catch_test_macros.hpp>

#include "felixbus/felixbus.hpp"

using namespace felixbus;

TEST_CASE( "multi publish data", "[error][server]" ) {

  char host[256];
  int rc = gethostname(host, 256);
  REQUIRE(rc == 0);
  std::string hostname(host);

  pid_t pid = getpid();
  REQUIRE(pid > 0);

  struct FelixBusInfo info;
  info.ip = "192.168.0.1";
  info.port = 12345;
  info.unbuffered = false;
  info.pubsub = true;
  info.raw_tcp = false;
  info.stream = false;
  info.netio_pages = 16;
  info.netio_pagesize = 256;

  uint64_t fid1 = 0x1f9f346f00000900;
  uint64_t fid2 = 0x1f9f368a00000a00;
  std::string filename = "test";

  std::filesystem::path bus_path_prefix = "unit_tests";
  bus_path_prefix /= "data";
  bus_path_prefix /= "multi_publish";

  FelixBus bus(bus_path_prefix);

  std::error_code ec;

  // first fid
  bus.publish(fid1, filename, info, ec);
  REQUIRE(!ec);

  // second fid
  bus.publish(fid2, filename, info, ec);
  REQUIRE(!ec);

  // lookup first fid
  FelixBusInfo check_info = bus.get_info(fid1, ec);
  REQUIRE(!ec);
  REQUIRE(check_info.host == hostname);
  REQUIRE(check_info.pid == pid);

  // lookup second fid
  check_info = bus.get_info(fid2, ec);
  REQUIRE(!ec);
  REQUIRE(check_info.host == hostname);
  REQUIRE(check_info.pid == pid);
}
