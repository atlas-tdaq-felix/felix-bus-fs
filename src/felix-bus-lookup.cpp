#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "docopt/docopt.h"

#include "felixbus/felixbus.hpp"

#include "felixtag.h"

using namespace felixbus;

static const char USAGE[] =
R"(felix-bus-lookup - Look up info on fids on the bus.

    Usage:
      felix-bus-lookup [options] <fids>...

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --verbose-bus                     Show bus information
      --bus-dir=<bus-directory>         Set bus directory [default: bus]
      --bus-groupname=<groupname>       Set groupname for bus to use [default: FELIX]
)";

int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string


  try {
    std::filesystem::path bus_path_prefix = args["--bus-dir"].asString();

    FelixBus bus(bus_path_prefix);
    bus.set_groupname(args["--bus-groupname"].asString());
    bus.set_verbose(args["--verbose-bus"].asBool());

    std::vector<std::string> fids = args["<fids>"].asStringList();
    for(std::vector<std::string>::iterator it = fids.begin(); it != fids.end(); ++it) {
      uint64_t fid = std::stoul(*it, 0, 0);
      std::error_code ec;
      FelixBusInfo info = bus.get_info(fid, ec);

      if (ec) {
        std::cout << ec.message() << " - " << ec << std::endl;
      } else {
        std::cout << info << std::endl;
      }
    }
  } catch (std::invalid_argument const& error) {
    std::cerr << "Argument or option of wrong type" << std::endl;
    std::cout << std::endl;
    std::cout << USAGE << std::endl;
    exit(-1);
  }
}
