#include <chrono>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "docopt/docopt.h"

#include "felixbus/felixbus.hpp"

#include "felixtag.h"

using namespace felixbus;

static const char USAGE[] =
R"(felix-bus-ping - Test to measure publish-lookup turnaround; to be used with felix-bus-pong.

    Usage:
      felix-bus-ping [options]

    Options:
      -h --help                         Show this screen.
      --version                         Show version.
      --verbose-bus                     Show bus information
      --bus-dir=<bus-directory>         Set bus directory [default: bus]
      --bus-groupname=<groupname>       Set groupname for bus to use [default: FELIX_PING_PONG]
      --no-cleanup                      Leave bus files in place
      --pingpong=<N>                    Number of times to ping-pong [default: 10]
)";

bool isEqual(const FelixBusInfo& info_pong, const FelixBusInfo& info_ping) {
  // fid, hfid, user, pid and host are from the pong channel
  if (info_pong.ip != info_ping.ip) return false;
  if (info_pong.port != info_ping.port) return false;
  if (info_pong.unbuffered != info_ping.unbuffered) return false;
  if (info_pong.pubsub != info_ping.pubsub) return false;
  if (info_pong.netio_pages != info_ping.netio_pages) return false;
  if (info_pong.netio_pagesize != info_ping.netio_pagesize) return false;
  return true;
}

int main(int argc, char** argv) {
    std::map<std::string, docopt::value> args
        = docopt::docopt(USAGE,
                         { argv + 1, argv + argc },
                         true,               // show help if requested
                         (std::string(argv[0]) + " " + FELIX_TAG).c_str());  // version string


  // Fake info
  FelixBusInfo info_ping;
  info_ping.ip = "192.168.0.1";
  info_ping.port = 12345;
  info_ping.unbuffered = false;
  info_ping.pubsub = true;
  info_ping.netio_pages = 16;
  info_ping.netio_pagesize = 0;

  try {
    std::filesystem::path bus_path_prefix = args["--bus-dir"].asString();

    FelixBus bus(bus_path_prefix);
    bus.set_groupname(args["--bus-groupname"].asString());
    bus.set_verbose(args["--verbose-bus"].asBool());
    bus.set_cleanup(!args["--no-cleanup"].asBool());

    std::string filename_ping = "ping";
    uint64_t fid_ping = 0x1ff0007000000700;

    // std::string filename_pong = "pong";
    uint64_t fid_pong = 0x1ff0007000000900;

    int no_of_ping_pong = args["--pingpong"].asLong();
    auto t0 = std::chrono::high_resolution_clock::now();

    while(no_of_ping_pong > 0) {
      std::cout << "Ping";
      auto tping = std::chrono::high_resolution_clock::now();
      bus.publish(fid_ping, filename_ping, info_ping);

      bool received = false;
      while(!received) {
        std::error_code ec;
        FelixBusInfo info_pong = bus.get_info(fid_pong, ec);

        if (!ec) {
          if (isEqual(info_pong, info_ping)) {
            std::cout << "-Pong: ";
            auto tpong = std::chrono::high_resolution_clock::now();
            auto ms_int = std::chrono::duration_cast<std::chrono::milliseconds>(tpong - tping);
            std::cout << ms_int.count() << "ms" << std::endl;
            received = true;
          }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(100));
      }

      // change a number
      info_ping.netio_pagesize++;

      no_of_ping_pong--;

      // std::this_thread::sleep_for(std::chrono::seconds(5));
    }

    auto t1 = std::chrono::high_resolution_clock::now();
    auto ms_int = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    std::cout << "Total: " << ms_int.count() << "ms" << std::endl;
  } catch (std::invalid_argument const& error) {
    std::cerr << "Argument or option of wrong type" << std::endl;
    std::cout << std::endl;
    std::cout << USAGE << std::endl;
    exit(-1);
  }
}
