#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <array>
#include <iostream>
#include <memory>
#include <vector>

#include "felixbus/felixbus.hpp"

namespace py = pybind11;
namespace fb = felixbus;

PYBIND11_MODULE(libfelix_bus_py, m) {
    m.doc() = "Python bindings for FelixBusClient.";

    // fix for usage of std::filesystem::path
    py::class_<std::filesystem::path>(m, "Path")
        .def(py::init<std::string>());
    py::implicitly_convertible<std::string, std::filesystem::path>();

    // struct FelixBusInfo
    py::class_<fb::FelixBusInfo, std::shared_ptr<fb::FelixBusInfo>> bus_info(m, "FelixBusInfo");
    // FelixBusInfo()
    bus_info.def(py::init<>())
        .def_readwrite("host", &fb::FelixBusInfo::host)
        .def_readwrite("pid", &fb::FelixBusInfo::pid)
        .def_readwrite("user", &fb::FelixBusInfo::user)

        .def_readwrite("hfid", &fb::FelixBusInfo::hfid)
        .def_readwrite("fid", &fb::FelixBusInfo::fid)
        .def_readwrite("ip", &fb::FelixBusInfo::ip)
        .def_readwrite("port", &fb::FelixBusInfo::port)
        .def_readwrite("unbuffered", &fb::FelixBusInfo::unbuffered)
        .def_readwrite("pubsub", &fb::FelixBusInfo::pubsub)
        .def_readwrite("netio_pages", &fb::FelixBusInfo::netio_pages)
        .def_readwrite("netio_pagesize", &fb::FelixBusInfo::netio_pagesize)

        .def_readwrite("raw_tcp", &fb::FelixBusInfo::raw_tcp)
        .def_readwrite("stream", &fb::FelixBusInfo::stream)
    ;

    // class FelixBus
    py::class_<fb::FelixBus, std::shared_ptr<fb::FelixBus>> bus(m, "FelixBus");
    // FelixBus()
    bus.def(py::init<>())
        // void publish(uint64_t fid, const std::string& filename, FelixBusInfo& info)
        .def("publish", py::overload_cast<uint64_t, const std::string&, fb::FelixBusInfo&>(&fb::FelixBus::publish),
            py::arg("fid"), py::arg("filename"), py::arg("info"))

        // void publish(uint64_t fid, const std::string& filename, FelixBusInfo& info, std::error_code& ec)
        .def("publish", py::overload_cast<uint64_t, const std::string&, fb::FelixBusInfo&, std::error_code&>(&fb::FelixBus::publish),
            py::arg("fid"), py::arg("filename"), py::arg("info"), py::arg("ec"))

        // FelixBusInfo get_info(uint64_t fid)
        .def("get_info", py::overload_cast<uint64_t>(&fb::FelixBus::get_info), py::arg("fid"))

        // FelixBusInfo get_info(uint64_t fid, std::error_code& ec)
        .def("get_info", py::overload_cast<uint64_t, std::error_code&>(&fb::FelixBus::get_info), py::arg("fid"), py::arg("ec"))

        // void set_path(std::filesystem::path path)
        .def("set_path", &fb::FelixBus::set_path, py::arg("path"))

        // void set_groupname(std::string groupname)
        .def("set_groupname", &fb::FelixBus::set_groupname, py::arg("groupname"))

        // void set_verbose(bool verbose)
        .def("set_verbose", &fb::FelixBus::set_verbose, py::arg("verbose"))
    ;
}
