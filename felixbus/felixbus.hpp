#pragma once

#include <filesystem>
#include <ostream>
#include <string>
#include <unordered_map>

#include "felixbus/felixbus.h"
#include "simdjson.h"

namespace felixbus {

  class FelixBusInfo {
    public:
      std::string host;
      pid_t pid;
      std::string user;

      uint64_t fid;
      std::string hfid;
      std::string ip = "";
      uint64_t port;
      bool unbuffered;
      bool pubsub;
      bool raw_tcp;
      bool stream = false;
      uint64_t netio_pages;
      uint64_t netio_pagesize;

      bool operator==(const FelixBusInfo& other) {
        if (host != other.host) return false;
        if (pid != other.pid) return false;
        if (user != other.user) return false;

        if (fid != other.fid) return false;
        if (hfid != other.hfid) return false;
        if (ip != other.ip) return false;
        if (port != other.port) return false;
        if (unbuffered != other.unbuffered) return false;
        if (pubsub != other.pubsub) return false;
        if (raw_tcp != other.raw_tcp) return false;
        if (stream != other.stream) return false;
        if (netio_pages != other.netio_pages) return false;
        if (netio_pagesize != other.netio_pagesize) return false;

        return true;
      }

      bool operator!=(const FelixBusInfo& other) {
        return !(*this == other);
      }

      friend std::ostream& operator<< (std::ostream& os, const FelixBusInfo& info) {
        os << "fid             " << info.fid << std::endl;
        os << "hfid            " << info.hfid << std::endl;
        os << "host:           " << info.host << std::endl;
        os << "pid:            " << info.pid << std::endl;
        os << "user:           " << info.user << std::endl;
        os << "ip:             " << info.ip << std::endl;
        os << "port:           " << info.port << std::endl;
        os << "unbuffered:     " << info.unbuffered << std::endl;
        os << "pubsub:         " << info.pubsub << std::endl;
        os << "raw_tcp:        " << info.raw_tcp << std::endl;
        os << "stream:         " << info.stream << std::endl;
        os << "netio_pages:    " << info.netio_pages << std::endl;
        os << "netio_pagesize: " << info.netio_pagesize << std::endl;
        return os;
      }
  };

  class FelixBus {

  public:
    explicit FelixBus(const std::string& path="bus", const std::string& groupname="FELIX", bool verbose=false, bool cleanup=true, bool ignore_stale=true) :
              bus_path_prefix(path), groupname(groupname), verbose(verbose), cleanup(cleanup), ignore_stale(ignore_stale) {
    }

    ~FelixBus() {
      publish_close();
    }

    void publish(uint64_t fid, const std::string& filename, FelixBusInfo& info) {
      publish(fid, filename, info, nullptr);
    }

    void publish(uint64_t fid, const std::string& filename, FelixBusInfo& info, std::error_code& ec) {
      publish(fid, filename, info, &ec);
    }

    void publish_close();

    FelixBusInfo get_info(uint64_t fid) {
      return get_info(fid, nullptr);
    }

    FelixBusInfo get_info(uint64_t fid, std::error_code& ec) {
      return get_info(fid, &ec);
    }

    void set_path(std::filesystem::path path) {
      this->bus_path_prefix = path;
    }

    void set_groupname(const std::string& groupname) {
      this->groupname = groupname;
    }

    void set_verbose(bool verbose) {
      this->verbose = verbose;
    }

    void set_cleanup(bool cleanup) {
      this->cleanup = cleanup;
    }

    void set_ignore_stale(bool ignore_stale) {
      this->ignore_stale = ignore_stale;
    }

    template <typename I> static std::string n2hexstr(I w, size_t hex_len = sizeof(I)<<1) {
      static const char* digits = "0123456789abcdef";
      std::string rc(hex_len,'0');
      for (size_t i=0, j=(hex_len-1)*4 ; i<hex_len; ++i,j-=4)
          rc[i] = digits[(w>>j) & 0x0f];
      return rc;
    }

    template <typename T> static std::string int_to_hex(T val, size_t width=sizeof(T)<<1) {
      std::stringstream ss;
      ss << std::setfill('0') << std::setw(width) << std::hex << (val|0);
      return ss.str();
    }

  private:
    void publish(uint64_t fid, const std::string& filename, FelixBusInfo& info, std::error_code* ecptr);
    FelixBusInfo get_info(uint64_t fid, std::error_code* ecptr);

    std::filesystem::path bus_path_prefix;
    std::string groupname;
    bool verbose;
    bool cleanup;
    bool ignore_stale;
    simdjson::dom::parser parser;

    std::unordered_map<std::string, felix_bus> bus_by_name;
  };
}
