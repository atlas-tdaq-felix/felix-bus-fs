#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

#define BUS_PID "pid"
#define BUS_HOST "host"
#define BUS_USER "user"

#define BUS_FID "fid"
#define BUS_HFID "hfid"
#define BUS_IP "ip"
#define BUS_PORT "port"
#define BUS_UNBUFFERED "unbuffered"
#define BUS_RAW_TCP "raw_tcp"
#define BUS_STREAM "stream"
#define BUS_PUBSUB "pubsub"
#define BUS_NETIO_PAGES "netio_pages"
#define BUS_NETIO_PAGESIZE "netio_pagesize"


struct felix_bus_info {
    const char* ip;
    uint32_t port;
    bool unbuffered;
    bool pubsub;
    bool raw_tcp;
    bool stream;
    uint32_t netio_pages;
    uint64_t netio_pagesize;
};

typedef struct felix_bus_s* felix_bus;

void felix_bus_set_verbose(int verbose);
void felix_bus_set_cleanup(int cleanup);

char* felix_bus_path(const char* bus_path_prefix, const char* groupname, uint8_t vid, uint8_t did, uint32_t cid, const char* bus_filename);

int felix_bus_locked(const char* bus_path);

int felix_bus_stale(const char* bus_path);

felix_bus felix_bus_open(const char* bus_path);

int felix_bus_write(felix_bus bus, uint64_t fid, const struct felix_bus_info* info);

int felix_bus_touch(felix_bus bus);

int felix_bus_close(felix_bus bus);

int felix_bus_release(felix_bus bus);

#ifdef __cplusplus
}
#endif
